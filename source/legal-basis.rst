.. _legal-basis:

Legal basis
===========

GDPR only allows processing of personal data if there is at least one legal
basis. The available legal bases are defined in Art. 6, Art. 9 GDPR. In the
context of Castellum, we consider a very limited set. Except for recruitment
consent, the legal basis is deduced from available information.

-   **Recruitment consent**: The subject has given explicit consent for being
    contacted for future studies. See :ref:`subject-consent`.

-   **Study consent**: This applies if the subject either participates in a
    study or is currently in the process of being recruited for one. This is
    valid until all pseudonym lists for the study have been deleted or (for
    subjects who are interested in news about the study) until the study is
    deleted.

-   **Legal representative**: As long as a subject is the legal representative
    for another subject it is assumed that the legal basis for the other
    subject extends to this one.

-   **Subject blocked**: In order to guarantee that a subject who has shown
    inappropriate behavior will not be invited to studies again, the fact
    *that* they are blocked can be stored without further consent.

If a subject does not have any legal basis for being in the database they will
appear in the :ref:`data protection dashboard <data-protection-dashboard>` so
their case can be reviewed and their data can be deleted from the system.
