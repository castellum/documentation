Features
========

Subject management
------------------

At its core, castellum can be used to record which subjects have participated
in which studies. This allows to keep track of all data related to a subject,
e.g. to export or delete all of it on request.

Relevant guides:

-   :ref:`subject-search`
-   :ref:`subject-create`
-   :ref:`subject-edit`
-   :ref:`subject-by-pseudonym`
-   :ref:`subject-by-general-pseudonym`
-   :ref:`subject-get-pseudonym`
-   :ref:`subject-to-be-deleted`
-   :ref:`subject-delete`
-   :ref:`subject-report`
-   :ref:`subject-block`
-   :ref:`subject-add-to-study`
-   :ref:`study-create`
-   :ref:`study-pseudonym-lists`
-   :ref:`study-delete-pseudonym-lists`
-   :ref:`study-delete`
-   :ref:`data-protection-dashboard`
-   :ref:`subject-export`

.. TODO::

    -   maintenance

Recruitment
-----------

It is possible to find potential subjects for a study from the pool of subjects
that have been entered into castellum. They can then be contacted either
manually or automatically via email. Subjects are only considered if they have
given consent to recruitment and if their recruitment attributes match the filters
that have been defined in the study.

Relevant guides:

-   :ref:`admin-consent-document`
-   :ref:`subject-consent`
-   :ref:`recruitment`
-   :ref:`recruitment-mail`
-   :ref:`study-recruitment-settings`
-   :ref:`study-filter-trial`
-   :ref:`admin-attributes`

Appointments
------------

Castellum can be used to manage appointments for test session. It will send
reminder mails to subjects automatically. You can also define resources such as
rooms or machines.

Relevant guides:

-   :ref:`study-sessions`
-   :ref:`recruitment-appointments`
-   :ref:`reception`
-   :ref:`set-up-external-scheduler`
-   :ref:`make-appointments-with-external-scheduler`
-   :ref:`access-calendar-feeds`

.. TODO::

    -   manage resources

Misc
----

.. TODO::

    -   custom groups
    -   news
