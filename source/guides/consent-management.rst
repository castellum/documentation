Managing recruitment consents
=============================

.. _subject-consent:

Set Recruitment Consent for a Subject
-------------------------------------

1.  In subject management > subject details, go to the **Data protection** tab.
    A box at the top shows the current consent. created subjects initially
    start with **No consent**.

2.  Click on the button **Change**.

3.  Select either **Consent declined/withdrawn** or the **consent document** the
    subject has agreed to. In the latter case, you can also upload a signed
    copy.

.. note::

    Please be aware that subject maintenance lists persons without recruitment
    consent. Hence, they may be deleted after some time waiting (if no other
    legal basis is given).


.. _subject-consent-maintenance:

Maintain Recruitment Consents
-----------------------------

To get an overview of all subjects for whom a new **Recruitment consent** is
required, proceed as follows:

1.  Click on **Subjects** on the front page

2.  Go to **Maintenance**

3.  Choose the **Consent** tab. You will see a list of all subjects with
    outdated recruitment consents.

4.  Click on **Details** of a listed subject (opens in new browser tab)

Refer to :ref:`subject-consent` once you have found a subject for which you
want to set consent.
